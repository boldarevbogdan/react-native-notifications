import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  Platform
} from 'react-native';
import {
  Permissions,
  Notifications
} from 'expo';


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: undefined,
      warningMsg: undefined
    }
  }

  async componentDidMount() {
    try {
      if (Platform.OS === 'android') {
        await Notifications.createChannelAndroidAsync('orders', {
          name: 'Name',
          sound: true,
          vibrate: [0, 250, 250, 250],
          priority: 'max',
        });
      }

      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
    
      // only ask if permissions have not already been determined, because
      // iOS won't necessarily prompt the user a second time.
      if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }
    
      // Stop here if the user did not grant permissions
      if (finalStatus !== 'granted') {
        return;
      }
    
      // Get the token that uniquely identifies this device
      let token = await Notifications.getExpoPushTokenAsync();
      this.setState({token: token});
    } catch(err) {
      this.setState({warningMsg: err});
    }
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.warningMsg !== undefined &&
        <View style={{margin: 20}}>
          <Text style={{color: 'red'}}>{this.state.warningMsg}</Text>
        </View>}
        <View style={{margin: 20}}>
          <Text>{this.state.token === undefined ? 'Token will appear here' : this.state.token}</Text>
        </View>
        <Button 
          onPress={this.onPress}
          title="Fetch!"
          color="#841584"
          />
      </View>
    );
  }

  onPress = async () => {
    try {
      let response = await fetch('http://192.168.0.102:3000/token', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          token: {
            value: this.state.token,
          },
          user: {
            username: 'Brent',
          },
          message: {
            value: 'this is updated app',
          },
        }),
      });
      console.log(response);
    } catch(err) {
      console.log(err);
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
